<?php

use Illuminate\Database\Seeder;

class TblIndicadoresTipoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_indicadores_tipo')->delete();
        
        \DB::table('tbl_indicadores_tipo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => ' Asistencial',
                'descripcion' => ' Asistencial',
                'color' => '',
                'nivel'=>'3'
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Administrativos y financieros',
                'descripcion' => 'Administrativos y financieros',
                'color' => '',
                'nivel'=>'2',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Dirección y Gerencia',
                'descripcion' => 'Dirección y Gerencia',
                'color' => '',
                'nivel'=>'1',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Externos',
                'descripcion' => 'Externos',
                'color' => '',
                'nivel'=>'4',
            ),
        ));
        
        
    }
}