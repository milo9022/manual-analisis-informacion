<?php

use Illuminate\Database\Seeder;

class TblRangosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_rangos')->delete();
        
        \DB::table('tbl_rangos')->insert(array (
            0 => 
            array (
                'id' => 1,
            'nombre' => 'Satisfactorio (Meta)',
            'descripcion' => 'Satisfactorio (Meta)',
                'color' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Aceptable',
                'descripcion' => 'Aceptable',
                'color' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Deficiente',
                'descripcion' => 'Deficiente',
                'color' => '',
            ),
        ));
        
        
    }
}