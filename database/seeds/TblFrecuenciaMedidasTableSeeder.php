<?php

use Illuminate\Database\Seeder;

class TblFrecuenciaMedidasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_frecuencia_medidas')->delete();
        
        \DB::table('tbl_frecuencia_medidas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Mensual',
                'descripcion' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Trimestral',
                'descripcion' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Semestral',
                'descripcion' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Anual',
                'descripcion' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}