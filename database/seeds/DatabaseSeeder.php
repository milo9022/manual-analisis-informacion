<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TblRangosTiposTableSeeder::class);
        $this->call(TblRangosTableSeeder::class);
        $this->call(TblIndicadoresTipoTableSeeder::class);
        $this->call(TblAreasTableSeeder::class);
        $this->call(TblFrecuenciaMedidasTableSeeder::class);
        $this->call(TblDocumentoTiposTableSeeder::class);
        $this->call(TblVigilanciaControlTableSeeder::class);
        $this->call(TblPuntosAtencionesTableSeeder::class);
        $this->call(TblPermisionsTableSeeder::class);
        $this->call(TblPermisionsRolesTableSeeder::class);
    }
}
