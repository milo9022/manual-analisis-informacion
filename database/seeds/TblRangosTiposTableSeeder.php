<?php

use Illuminate\Database\Seeder;

class TblRangosTiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_rangos_tipos')->delete();
        
        \DB::table('tbl_rangos_tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'mayor que',
                'descripcion' => 'Si el valor es mayor',
                'caracter' => '>',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Menor que',
                'descripcion' => 'Si el valor es menor',
                'caracter' => '<',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Igual que',
                'descripcion' => 'Si el valor es igual',
                'caracter' => '=',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Mayor o igual que',
                'descripcion' => 'Si el valor es mayo o igual',
                'caracter' => '>=',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Menor o igual que',
                'descripcion' => 'Si el valor es menor o igual',
                'caracter' => '<=',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Diferente',
                'descripcion' => 'Si el valor es diferente',
                'caracter' => '!=',
            ), 
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Rango',
                'descripcion' => 'Rangos de medida',
                'caracter' => 'Rango',
            ),
        ));
        
        
    }
}