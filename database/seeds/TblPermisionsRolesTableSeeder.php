<?php

use Illuminate\Database\Seeder;

class TblPermisionsRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_permisions_roles')->delete();
        
        \DB::table('tbl_permisions_roles')->insert(array (
            0 => 
            array (
                'id' => 7,
                'id_rol' => 2,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 21:41:13',
                'updated_at' => '2020-01-24 21:41:13',
            ),
            1 => 
            array (
                'id' => 8,
                'id_rol' => 1,
                'id_permisions' => 1,
                'created_at' => '2020-01-24 21:50:21',
                'updated_at' => '2020-01-24 21:50:21',
            ),
            2 => 
            array (
                'id' => 9,
                'id_rol' => 1,
                'id_permisions' => 2,
                'created_at' => '2020-01-24 21:50:25',
                'updated_at' => '2020-01-24 21:50:25',
            ),
            3 => 
            array (
                'id' => 10,
                'id_rol' => 1,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 21:50:28',
                'updated_at' => '2020-01-24 21:50:28',
            ),
            4 => 
            array (
                'id' => 11,
                'id_rol' => 1,
                'id_permisions' => 4,
                'created_at' => '2020-01-24 21:50:29',
                'updated_at' => '2020-01-24 21:50:29',
            ),
            5 => 
            array (
                'id' => 12,
                'id_rol' => 1,
                'id_permisions' => 8,
                'created_at' => '2020-01-24 21:50:29',
                'updated_at' => '2020-01-24 21:50:29',
            ),
            6 => 
            array (
                'id' => 13,
                'id_rol' => 1,
                'id_permisions' => 7,
                'created_at' => '2020-01-24 21:50:30',
                'updated_at' => '2020-01-24 21:50:30',
            ),
            7 => 
            array (
                'id' => 14,
                'id_rol' => 1,
                'id_permisions' => 6,
                'created_at' => '2020-01-24 21:50:32',
                'updated_at' => '2020-01-24 21:50:32',
            ),
            8 => 
            array (
                'id' => 15,
                'id_rol' => 1,
                'id_permisions' => 5,
                'created_at' => '2020-01-24 21:50:33',
                'updated_at' => '2020-01-24 21:50:33',
            ),
            9 => 
            array (
                'id' => 16,
                'id_rol' => 1,
                'id_permisions' => 9,
                'created_at' => '2020-01-24 21:50:33',
                'updated_at' => '2020-01-24 21:50:33',
            ),
            10 => 
            array (
                'id' => 17,
                'id_rol' => 1,
                'id_permisions' => 10,
                'created_at' => '2020-01-24 21:50:34',
                'updated_at' => '2020-01-24 21:50:34',
            ),
            11 => 
            array (
                'id' => 18,
                'id_rol' => 1,
                'id_permisions' => 11,
                'created_at' => '2020-01-24 21:50:35',
                'updated_at' => '2020-01-24 21:50:35',
            ),
            12 => 
            array (
                'id' => 19,
                'id_rol' => 1,
                'id_permisions' => 12,
                'created_at' => '2020-01-24 21:50:36',
                'updated_at' => '2020-01-24 21:50:36',
            ),
            13 => 
            array (
                'id' => 20,
                'id_rol' => 1,
                'id_permisions' => 16,
                'created_at' => '2020-01-24 21:50:36',
                'updated_at' => '2020-01-24 21:50:36',
            ),
            14 => 
            array (
                'id' => 21,
                'id_rol' => 1,
                'id_permisions' => 15,
                'created_at' => '2020-01-24 21:50:37',
                'updated_at' => '2020-01-24 21:50:37',
            ),
            15 => 
            array (
                'id' => 22,
                'id_rol' => 1,
                'id_permisions' => 14,
                'created_at' => '2020-01-24 21:50:38',
                'updated_at' => '2020-01-24 21:50:38',
            ),
            16 => 
            array (
                'id' => 23,
                'id_rol' => 1,
                'id_permisions' => 13,
                'created_at' => '2020-01-24 21:50:39',
                'updated_at' => '2020-01-24 21:50:39',
            ),
            17 => 
            array (
                'id' => 24,
                'id_rol' => 1,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 21:50:39',
                'updated_at' => '2020-01-24 21:50:39',
            ),
            18 => 
            array (
                'id' => 25,
                'id_rol' => 1,
                'id_permisions' => 18,
                'created_at' => '2020-01-24 21:50:40',
                'updated_at' => '2020-01-24 21:50:40',
            ),
            19 => 
            array (
                'id' => 26,
                'id_rol' => 1,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 21:50:41',
                'updated_at' => '2020-01-24 21:50:41',
            ),
            20 => 
            array (
                'id' => 27,
                'id_rol' => 3,
                'id_permisions' => 1,
                'created_at' => '2020-01-24 22:53:58',
                'updated_at' => '2020-01-24 22:53:58',
            ),
            21 => 
            array (
                'id' => 28,
                'id_rol' => 3,
                'id_permisions' => 2,
                'created_at' => '2020-01-24 22:53:59',
                'updated_at' => '2020-01-24 22:53:59',
            ),
            22 => 
            array (
                'id' => 29,
                'id_rol' => 3,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 22:54:00',
                'updated_at' => '2020-01-24 22:54:00',
            ),
            23 => 
            array (
                'id' => 31,
                'id_rol' => 3,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 22:54:13',
                'updated_at' => '2020-01-24 22:54:13',
            ),
            24 => 
            array (
                'id' => 32,
                'id_rol' => 3,
                'id_permisions' => 18,
                'created_at' => '2020-01-24 22:54:14',
                'updated_at' => '2020-01-24 22:54:14',
            ),
            25 => 
            array (
                'id' => 33,
                'id_rol' => 3,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 22:54:15',
                'updated_at' => '2020-01-24 22:54:15',
            ),
            26 => 
            array (
                'id' => 34,
                'id_rol' => 4,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 22:54:35',
                'updated_at' => '2020-01-24 22:54:35',
            ),
            27 => 
            array (
                'id' => 35,
                'id_rol' => 4,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 22:54:54',
                'updated_at' => '2020-01-24 22:54:54',
            ),
            28 => 
            array (
                'id' => 36,
                'id_rol' => 4,
                'id_permisions' => 18,
                'created_at' => '2020-01-24 22:54:55',
                'updated_at' => '2020-01-24 22:54:55',
            ),
            29 => 
            array (
                'id' => 37,
                'id_rol' => 4,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 22:54:56',
                'updated_at' => '2020-01-24 22:54:56',
            ),
        ));
        
        
    }
}