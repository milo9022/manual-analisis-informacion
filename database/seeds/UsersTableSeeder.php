<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $role_user = Role::where('name', 'usuario')->first();
        $role_admin = Role::where('name', 'admin')->first();
        
        $user = new User();
        $user->nombre_primero = 'admin';
        $user->nombre_segundo = 'admin';
        $user->apellido_primero = 'admin';
        $user->apellido_segundo = 'admin';
        $user->documento = '1';
        $user->activo = 1;
        $user->id_punto_atencion = 1;
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach($role_admin);
        
        unset($user);

        $user = new User();
        $user->nombre_primero = 'admin2';
        $user->nombre_segundo = 'admin2';
        $user->apellido_primero = 'admin2';
        $user->apellido_segundo = 'admin2';
        $user->documento = '2';
        $user->id_punto_atencion = 1;
        $user->activo = 1;
        $user->email = 'admin2@admin2.com';
        $user->password = bcrypt('admin2');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach($role_user);
     }
}