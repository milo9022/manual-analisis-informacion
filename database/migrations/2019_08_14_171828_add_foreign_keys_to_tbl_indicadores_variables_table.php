<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblIndicadoresVariablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_indicadores_variables', function(Blueprint $table)
		{
			$table->foreign('id_indicador', 'fk_tbl_indicadores_tbl_indicadores_variables')->references('id')->on('tbl_indicadores')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_indicadores_variables', function(Blueprint $table)
		{
			$table->dropForeign('fk_tbl_indicadores_tbl_indicadores_variables');
		});
	}

}
