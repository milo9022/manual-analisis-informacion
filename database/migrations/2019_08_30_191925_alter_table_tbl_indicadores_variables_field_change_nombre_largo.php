<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblIndicadoresVariablesFieldChangeNombreLargo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_indicadores_variables', function (Blueprint $table) {
            $table->dropColumn(['nombre largo']);
            $table->string('nombre_largo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_indicadores_variables', function (Blueprint $table) {
            //
        });
    }
}
