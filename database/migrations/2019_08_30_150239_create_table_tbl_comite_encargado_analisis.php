<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblComiteEncargadoAnalisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_comite_encargado_analisis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo')->nullable();
            $table->string('apellid_primero');
            $table->string('apellid_segundo')->nullable();
            $table->integer('id_documento_tipo');
            $table->string('documento');
            $table->integer('id_area');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_comite_encargado_analisis');
    }
}
