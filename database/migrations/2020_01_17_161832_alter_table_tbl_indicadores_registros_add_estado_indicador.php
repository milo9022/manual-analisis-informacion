<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblIndicadoresRegistrosAddEstadoIndicador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_indicadores_registros', function (Blueprint $table) {
            $table->enum('valor_estado', ['aceptable', 'deficiente','satisfactorio'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_indicadores_registros', function (Blueprint $table) {
            //
        });
    }
}
