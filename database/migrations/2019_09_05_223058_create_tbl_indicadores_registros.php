<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblIndicadoresRegistros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_indicadores_registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('valor')->nullable();
            $table->integer('anno')->nullable();
            $table->integer('mes')->nullable();
            $table->integer('id_indicador');
            $table->string('url_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_indicadores_registros');
    }
}
