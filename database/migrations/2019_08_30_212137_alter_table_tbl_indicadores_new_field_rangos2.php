<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblIndicadoresNewFieldRangos2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_indicadores', function (Blueprint $table) {
            $table->double('rango_aceptable_valor2')->nullable();
            $table->double('rango_safistactorio_valor2')->nullable();
            $table->double('rango_deficiente_valor2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_indicadores', function (Blueprint $table) {
            //
        });
    }
}
