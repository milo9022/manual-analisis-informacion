<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblIndicadoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbl_indicadores', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('numero')->unsigned();
			$table->string('codigo');
			$table->bigInteger('id_area')->unsigned()->index('fk_tbl_areas_tbl_indicadores');
			$table->string('nombre');
			$table->bigInteger('id_indicador_tipo')->unsigned()->index('fk_tbl_indicadores_tipo_tbl_indicadores');
			$table->text('justificacion');
			$table->text('normativa');
			$table->text('alcance');
			$table->text('unidad_medida');
			$table->integer('id_frecuencia_medida')->unsigned();
			$table->text('numerador');
			$table->text('fuente_numerador');
			$table->text('fuente_denominador');
			$table->text('formula');
			$table->integer('id_responsable');
			$table->integer('id_comite_encargado_analisis')->unsigned();
			$table->integer('id_vigilancia_control')->unsigned();
			$table->string('rango_aceptable_valor');
			$table->bigInteger('id_rango_aceptable_tipo')->unsigned()->index('fk_tbl_rango_tipo_tbl_indicadores');
			$table->string('rango_safistactorio_valor');
			$table->integer('id_rango_satisfactorio_tipo');
			$table->string('rango_deficiente_valor');
			$table->string('formula_texto');
			$table->integer('id_rango_deficiente_tipo');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbl_indicadores');
	}

}
