<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblIndicadoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_indicadores', function(Blueprint $table)
		{
			$table->foreign('id_area', 'fk_tbl_areas_tbl_indicadores')->references('id')->on('tbl_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_indicador_tipo', 'fk_tbl_indicadores_tipo_tbl_indicadores')->references('id')->on('tbl_indicadores_tipo')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_rango_aceptable_tipo', 'fk_tbl_rango_tipo_tbl_indicadores')->references('id')->on('tbl_rangos_tipos')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_indicadores', function(Blueprint $table)
		{
			$table->dropForeign('fk_tbl_areas_tbl_indicadores');
			$table->dropForeign('fk_tbl_indicadores_tipo_tbl_indicadores');
			$table->dropForeign('fk_tbl_rango_tipo_tbl_indicadores');
		});
	}

}
