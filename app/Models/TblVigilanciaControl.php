<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblVigilanciaControl extends Model
{
    use SoftDeletes; //Implementamos 

    protected $table = 'tbl_vigilancia_control';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['id','nombre'];
}
