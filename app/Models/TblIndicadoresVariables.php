<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblIndicadoresVariables extends Model
{
    use SoftDeletes; //Implementamos 

    protected $table = 'tbl_indicadores_variables';
    protected $fillable = ['id','id_indicador','nombre_corto','nombre_largo'];
}
