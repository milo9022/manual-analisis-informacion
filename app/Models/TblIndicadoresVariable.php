<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
/**
 * Class TblIndicadoresVariable
 * 
 * @property int $id
 * @property string $nombre_corto
 * @property string $nombre largo
 * @property int $id_indicador
 * @property float $valor
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\TblIndicadore $tbl_indicadore
 *
 * @package App\Models
 */
class TblIndicadoresVariable extends Eloquent
{
	use SoftDeletes; //Implementamos 

	protected $casts = [
		'id_indicador' => 'int',
		'valor' => 'float'
	];

	protected $fillable = [
		'nombre_corto',
		'nombre largo',
		'id_indicador',
		'valor'
	];

	public function tbl_indicadore()
	{
		return $this->belongsTo(\App\Models\TblIndicadore::class, 'id_indicador');
	}
}
