<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
/**
 * Class TblIndicadoresTipo
 * 
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $color
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $tbl_indicadores
 *
 * @package App\Models
 */
class TblIndicadoresTipo extends Eloquent
{
	use SoftDeletes; //Implementamos 

	protected $table = 'tbl_indicadores_tipo';
	protected $hidden = ['created_at', 'updated_at'];
	protected $fillable = [
		'nombre',
		'descripcion',
		'color'
	];

	public function tbl_indicadores()
	{
		return $this->hasMany(\App\Models\TblIndicadore::class, 'id_indicador_tipo');
	}
}
