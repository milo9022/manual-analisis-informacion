<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPuntosAtenciones extends Model
{
    protected $table = 'tbl_punto_atenciones';

    protected $primaryKey = 'id';

    protected $hidden = ['created_at', 'updated_at'];
    
    protected $fillable = ['nombre','id_municipio'];

    protected $dates = [];
    
    protected $casts = [];
    
    public function TblMunicipio()
    {
        return $this->belongsTo('App\Models\TblMunicipios','id_municipio','id');
    }
}
