<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblFrecuenciaMedidas extends Model
{
	use SoftDeletes; //Implementamos 

	protected $table = 'tbl_frecuencia_medidas';
	protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
		'nombre',
		'descripcion'
	];
}
