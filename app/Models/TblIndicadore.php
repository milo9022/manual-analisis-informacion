<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
/**
 * Class TblIndicadore
 * 
 * @property int $id
 * @property int $numero
 * @property string $codigo
 * @property int $id_area
 * @property string $nombre
 * @property int $id_indicador_tipo
 * @property string $justificacion
 * @property string $normativa
 * @property string $alcance
 * @property string $unidad_medida
 * @property int $id_frecuencia_medida
 * @property string $numerador
 * @property string $fuente_numerador
 * @property string $fuente_denominador
 * @property string $formula
 * @property int $id_responsable
 * @property int $id_comite_encargado_analisis
 * @property int $id_vigilancia_control
 * @property string $rango_aceptable_valor
 * @property int $id_rango_aceptable_tipo
 * @property string $rango_safistactorio_valor
 * @property int $id_rango_satisfactorio_tipo
 * @property string $rango_deficiente_valor
 * @property int $id_rango_satisfactorio_tip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\TblArea $tbl_area
 * @property \App\Models\TblIndicadoresTipo $tbl_indicadores_tipo
 * @property \App\Models\TblRangosTipo $tbl_rangos_tipo
 * @property \Illuminate\Database\Eloquent\Collection $tbl_indicadores_variables
 *
 * @package App\Models
 */
class TblIndicadore extends Eloquent
{
	use SoftDeletes; //Implementamos 

	protected $casts = [
		'numero' => 'int',
		'id_area' => 'int',
		'id_indicador_tipo' => 'int',
		'id_frecuencia_medida' => 'int',
		'id_responsable' => 'int',
		'id_comite_encargado_analisis' => 'int',
		'id_vigilancia_control' => 'int',
		'id_rango_aceptable_tipo' => 'int',
		'id_rango_satisfactorio_tipo' => 'int',
		'id_rango_satisfactorio_tip' => 'int'
	];

	protected $fillable = [
		'numero',
		'codigo',
		'id_area',
		'nombre',
		'id_indicador_tipo',
		'justificacion',
		'normativa',
		'alcance',
		'unidad_medida',
		'id_frecuencia_medida',
		'numerador',
		'fuente_numerador',
		'fuente_denominador',
		'formula',
		'id_responsable',
		'id_comite_encargado_analisis',
		'id_vigilancia_control',
		'rango_aceptable_valor',
		'id_rango_aceptable_tipo',
		'rango_safistactorio_valor',
		'id_rango_satisfactorio_tipo',
		'rango_deficiente_valor',
		'id_rango_deficiente_tipo'
	];
	
	protected $table = 'tbl_indicadores';

	public function tbl_area()
	{
		return $this->belongsTo(\App\Models\TblArea::class, 'id_area');
	}

	public function tbl_indicadores_tipo()
	{
		return $this->belongsTo(\App\Models\TblIndicadoresTipo::class, 'id_indicador_tipo');
	}

	#Rangos
	public function tbl_rangos_tipo_satisfactorio()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_satisfactorio_tipo');
	}
	public function tbl_rangos_tipo_satisfactorio2()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_satisfactorio_tipo2');
	}
	
	
	public function tbl_rangos_tipo_aceptable()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_aceptable_tipo');
	}
	public function tbl_rangos_tipo_aceptable2()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_aceptable_tipo2');
	}

	public function tbl_rangos_tipo_deficiente()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_deficiente_tipo');
	}
	public function tbl_rangos_tipo_deficiente2()
	{
		return $this->belongsTo(\App\Models\TblRangosTipo::class, 'id_rango_deficiente_tipo2');
	}
	#Rangos


	public function tbl_indicadores_variables()
	{
		return $this->hasMany(\App\Models\TblIndicadoresVariable::class, 'id_indicador');
	}
	public function tbl_frecuencia_medida()
	{
		return $this->belongsTo(\App\Models\TblFrecuenciaMedidas::class, 'id_frecuencia_medida');
	}
	public function tbl_responsable_analisis()
	{
		return $this->belongsTo(\App\Models\TblArea::class, 'id_responsable');
	}
	public function tbl_responsable_reporte()
	{
		return $this->belongsTo(\App\User::class, 'id_comite_encargado_analisis');
	}
	public function tbl_responsable_vigilancia()
	{
		return $this->belongsTo(\App\Models\TblVigilanciaControl::class, 'id_vigilancia_control');
	}	
}
