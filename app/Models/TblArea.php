<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
/**
 * Class TblArea
 * 
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $tbl_indicadores
 *
 * @package App\Models
 */
class TblArea extends Eloquent
{
	use SoftDeletes; //Implementamos 

	protected $fillable = [
		'nombre',
		'descripcion'
	];
	protected $hidden = ['created_at', 'updated_at'];

	public function tbl_indicadores()
	{
		return $this->hasMany(\App\Models\TblIndicadore::class, 'id_area');
	}
}
