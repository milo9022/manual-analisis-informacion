<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblDocumentoTipo extends Model
{
    use SoftDeletes; //Implementamos 

    protected $table = 'tbl_documento_tipos';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['id','nombre','nombre_corto'];
}
