<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblIndicadoresRegistros extends Model
{
	use SoftDeletes; //Implementamos 

    protected $table = 		'tbl_indicadores_registros';
	protected $hidden = 	['created_at', 'updated_at'];
    protected $fillable = 	['id','valor','anno','mes','id_indicador','url_file','tbl_indicadores_registros','valor_estado','id_user'];
    public function tbl_indicadore()
	{
		return $this->belongsTo(\App\Models\TblIndicadore::class, 'id_indicador');
	}
	public function tbl_usuario()
	{
		return $this->belongsTo(\App\User::class, 'id_user');
	}
	
}
