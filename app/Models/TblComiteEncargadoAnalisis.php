<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
class TblComiteEncargadoAnalisis extends Model
{
    use SoftDeletes; //Implementamos 
    protected $table = 'tbl_comite_encargado_analisis';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
		    'id',
        'nombre_primero',
        'nombre_segundo',
        'apellid_primero',
        'apellid_segundo',
        'id_documento_tipo',
        'documento',
        'id_area',
        'email',
    ];
    public function tblArea()
	{
		return $this->belongsTo(\App\Models\TblArea::class, 'id_area');
    }
    public function tblDocumentoTipo()
	{
		return $this->belongsTo(\App\Models\TblDocumentoTipo::class, 'id_documento_tipo');
    }
    
}
