<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
/**
 * Class TblRango
 * 
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $color
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class TblRango extends Eloquent
{
	use SoftDeletes; //Implementamos 

	protected $fillable = [
		'nombre',
		'descripcion',
		'color'
	];
	protected $hidden = ['created_at', 'updated_at'];
}
