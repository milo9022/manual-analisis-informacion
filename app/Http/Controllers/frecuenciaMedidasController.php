<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblFrecuenciaMedidas;

class frecuenciaMedidasController extends Controller
{
    public function AllFrecuencia()
    {
        return TblFrecuenciaMedidas::all();
    }
    public function borrar($id)
    {
        try {
            $data = TblFrecuenciaMedidas::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
