<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblIndicadore;
use App\Http\Controllers\DataController;
use App\Models\TblIndicadoresTipo;
use App\Models\User;
use App\Models\TblIndicadoresVariables;
use App\Models\TblIndicadoresRegistros;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class indicadoresController extends Controller
{
    public function AllIndicadores(Request $request)
    {
        $data= TblIndicadore::
        with('tbl_area')->
        with('tbl_indicadores_tipo')->
        with('tbl_frecuencia_medida')->
        with('tbl_rangos_tipo_satisfactorio')->
        with('tbl_rangos_tipo_aceptable')->

        with('tbl_responsable_analisis')->
        with('tbl_responsable_reporte')->
        with('tbl_responsable_vigilancia')->

        with('tbl_rangos_tipo_deficiente');
        if(isset($request->page))
        {
            try 
            {
                $user = User::where('id','=',Auth::user()->id)->with('roles')->get()->first();
                if($user->roles[0]->id==2)
                {
                    $data->where('id_comite_encargado_analisis','=',Auth::user()->id);
                }
                $limit=$request->registros;
                $registro=($limit*$request->page)-$limit;
                $data->limit($limit)->offset($registro);
                $total=TblIndicadore::count();
                return ['data'=>$data->get(),'page'=>$request->page,'pages'=>ceil($total/$limit),'total'=>$total];
            } catch (\Throwable $th) {
                return [];
            }

        }
        else
        {
            return $data->get();
        }
    }
    private function evaluarEstadoRegistro($valor,$cat1,$cat2,$tipo,$res)
    {
        $valor = (double) $valor;
        $cat1 = (double) $cat1;
        $cat2 = (double) $cat2;
        switch($tipo)
        {
            case 1:
                $res=($valor>$cat1)?$res:0;
            break;
            case 2:
                $res=($valor<$cat1)?$res:0;
            break;
            case 3:
                $res=($valor==$cat1)?$res:0;
            break;
            case 4:
                $res=($valor>=$cat1)?$res:0;
            break;
            case 5:
                $res=($valor<=$cat1)?$res:0;
            break;
            case 6:
                $res=($valor!=$cat1)?$res:0;
            break;
            case 7:
            $res=($valor>=$cat1&&$valor<=$cat2)?$res:0;
            break;
        }
        return $res;
    }
    private function estadoRegistro(&$data)
    {
        foreach($data as $key => $temp)
        {
            if(is_null($temp['tbl_indicadore'])){
                unset($data[$key]);
            }
            else{
                $estado=array();
                $valor=$temp->valor;
                $datas=(object)$temp['tbl_indicadore'];
                $estado[] = $this->evaluarEstadoRegistro($temp->valor,$datas->rango_safistactorio_valor,$datas->rango_safistactorio_valor2,$datas->id_rango_satisfactorio_tipo,1);
                $estado[] = $this->evaluarEstadoRegistro($temp->valor,$datas->rango_aceptable_valor,    $datas->rango_aceptable_valor2,    $datas->id_rango_aceptable_tipo,2);
                $estado[] = $this->evaluarEstadoRegistro($temp->valor,$datas->rango_deficiente_valor,   $datas->rango_deficiente_valor2,   $datas->id_rango_deficiente_tipo,3);
                $data[$key]->estadoValorRegistro=max($estado);
            }
        }
        return $data;
    }
    /*
    */
    public function AllRegistros(Request $request)
    {
        $data= TblIndicadoresRegistros::with('tbl_indicadore.tbl_area')->with('tbl_usuario')->with('tbl_usuario.TblPuntosAtencion')->with('tbl_usuario.Tbl_areas')->orderBy('anno');
        if(isset($request->page))
        {
            try 
            {
                $limit=$request->registros;
                $registro=($limit*$request->page)-$limit;
                $data->limit($limit)->offset($registro);
                $data->orderBy('id_indicador');
                $data->orderBy('mes');
                $data=$data->get();
                $this->estadoRegistro($data);
                $this->formatData($data);
                $total=TblIndicadoresRegistros::count();
                return ['data'=>$data,'page'=>$request->page,'pages'=>ceil($total/$limit),'total'=>$total];
            } catch (\Exception $th) {
                return ['error'=>$th->getMessage()];
            }
        }
        else
        {
            $res = $data->get();
            return $res;
        }
    }
    private function formatData(&$data)
    {
        $res    = json_decode(json_encode($data));
        $res2=array();
        foreach($res as $key => $temp)
        {
            $key_anno     = array_search($temp->anno, array_column($res2, 'key'));
            if($key_anno===false)
            {
                $res2[]=[
                    'key'=>$temp->anno,
                    'indicador'=>[]
                ];
                $key_anno     = array_search($temp->anno, array_column($res2, 'key'));
            }
            $key_indicador = array_search($temp->id_indicador, array_column($res2[$key_anno]['indicador'], 'id'));
            if($key_indicador===false)
            {
                $res2[$key_anno]['indicador'][]=[
                    'id'=>$temp->id_indicador,
                    'nombre'=>$temp->tbl_indicadore->nombre,
                    'tbl_indicadores'=>$temp->tbl_indicadore,
                    'value'=>array()
                ];
                $key_indicador = array_search($temp->id_indicador, array_column($res2[$key_anno]['indicador'], 'id'));
            }
            $res2[$key_anno]['indicador'][$key_indicador]['value'][]=$temp;
        }
        $data = $res2;
    }
    public function Load(Request $request)
    {
        try{

            $data               = TblIndicadore::findOrFail($request->id);
            $res                = TblIndicadoresVariables::where('id_indicador','=',$request->id)->get();
            $data->variables    = (is_null($res)) ? []:$res;
            return ['validate'=>true,'data'=>$data,'msj'=>null];
        }
        catch(\Exception $e){
            return  ['validate'=>false,'data'=>[],'msj'=>$e->getMessage()];
        }
    }
    public function Save(Request $request)
    { 
        try 
        {
            $data = new TblIndicadore();
            $data->numero                           = $request->numero;
            $data->codigo                           = $request->codigo;
            $data->id_area                          = $request->id_area;
            $data->nombre                           = $request->nombre;
            $data->id_indicador_tipo                = $request->id_indicador_tipo;
            $data->justificacion                    = $request->justificacion;
            $data->normativa                        = $request->normativa;
            $data->alcance                          = $request->alcance;
            $data->unidad_medida                    = $request->unidad_medida;
            $data->id_frecuencia_medida             = $request->id_frecuencia_medida;
            $data->numerador                        = $request->numerador;
            $data->fuente_numerador                 = $request->fuente_numerador;
            $data->fuente_denominador               = $request->fuente_denominador;
            $data->formula                          = $request->formula;
            $data->id_responsable                   = $request->id_responsable;
            $data->id_comite_encargado_analisis     = $request->id_comite_encargado_analisis;
            $data->id_vigilancia_control            = $request->id_vigilancia_control;
            $data->denominador                      = $request->denominador;
            $data->formula_texto                    = $request->formula_texto;
            
            $data->rango_aceptable_valor            = $request->rango_aceptable_valor;
            $data->rango_aceptable_valor2           = $request->rango_aceptable_valor2;
            $data->rango_safistactorio_valor        = $request->rango_safistactorio_valor;
            $data->rango_safistactorio_valor2       = $request->rango_safistactorio_valor2;
            $data->rango_deficiente_valor           = $request->rango_deficiente_valor;
            $data->rango_deficiente_valor2          = $request->rango_deficiente_valor2;

            $data->id_rango_aceptable_tipo          = $request->id_rango_aceptable_tipo;
            $data->id_rango_satisfactorio_tipo      = $request->id_rango_satisfactorio_tipo;
            $data->id_rango_deficiente_tipo         = $request->id_rango_deficiente_tipo;
            $data->id_punto_atencion                = Auth::user()->id_punto_atencion;
            $data->save();
            foreach($request->variables as $temp)
            {
                $res = new TblIndicadoresVariables();
                $res->id_indicador=$data->id;
                $res->nombre_corto=$temp['nombre_corto'];
                $res->nombre_largo=$temp['nombre_largo'];
                $res->save();
            }
            return ['validate'=>true,'msj'=>null];
        } 
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'msj'=>$th->getMessage()];
        }
    }
    public function update(Request $request)
    { 
        try 
        {
            $data = TblIndicadore::find($request->id);
            $data->numero                           = $request->numero;
            $data->codigo                           = $request->codigo;
            $data->id_area                          = $request->id_area;
            $data->nombre                           = $request->nombre;
            $data->id_indicador_tipo                = $request->id_indicador_tipo;
            $data->justificacion                    = $request->justificacion;
            $data->normativa                        = $request->normativa;
            $data->alcance                          = $request->alcance;
            $data->unidad_medida                    = $request->unidad_medida;
            $data->id_frecuencia_medida             = $request->id_frecuencia_medida;
            $data->numerador                        = $request->numerador;
            $data->fuente_numerador                 = $request->fuente_numerador;
            $data->fuente_denominador               = $request->fuente_denominador;
            $data->formula                          = $request->formula;
            $data->id_responsable                   = $request->id_responsable;
            $data->id_comite_encargado_analisis     = $request->id_comite_encargado_analisis;
            $data->id_vigilancia_control            = $request->id_vigilancia_control;
            $data->denominador                      = $request->denominador;
            $data->formula_texto                    = $request->formula_texto;
            
            $data->rango_aceptable_valor            = $request->rango_aceptable_valor;
            $data->rango_aceptable_valor2           = $request->rango_aceptable_valor2;
            $data->rango_safistactorio_valor        = $request->rango_safistactorio_valor;
            $data->rango_safistactorio_valor2       = $request->rango_safistactorio_valor2;
            $data->rango_deficiente_valor           = $request->rango_deficiente_valor;
            $data->rango_deficiente_valor2          = $request->rango_deficiente_valor2;

            $data->id_rango_aceptable_tipo          = $request->id_rango_aceptable_tipo;
            $data->id_rango_satisfactorio_tipo      = $request->id_rango_satisfactorio_tipo;
            $data->id_rango_deficiente_tipo         = $request->id_rango_deficiente_tipo;
            
            $data->save();
            TblIndicadoresVariables::where('id_indicador','=',$request->id)->delete();
            foreach($request->variables as $temp)
            {
                $res = new TblIndicadoresVariables();
                $res->id_indicador=$data->id;
                $res->nombre_corto=$temp['nombre_corto'];
                $res->nombre_largo=$temp['nombre_largo'];
                $res->save();
            }
            return ['validate'=>true,'msj'=>null];
        } 
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'msj'=>$th->getMessage()];
        }
    }
    private function CalcularVariables($id_indicador,$registros)
    {
        $res = TblIndicadore::find($id_indicador);
        $formula = $res->formula;
        $data=array();
        foreach($registros as $temp)
        {
            $var=TblIndicadoresVariables::find($temp->id);
            $data[$var->nombre_corto]=$temp->value;
        }
        $datas=(object)['formula'=>$formula,'parametros'=>$data];
        return ($datas);
    }
    /*
    */
    public function valor_estado($data)
    {
        $res      = TblIndicadore::find($data->id_indicador);
        $estado   = array();
        $estado[] = $this->evaluarEstadoRegistro($data->valor,  $res->rango_safistactorio_valor,$res->rango_safistactorio_valor2,$res->id_rango_satisfactorio_tipo,1);
        $estado[] = $this->evaluarEstadoRegistro($data->valor,  $res->rango_aceptable_valor,    $res->rango_aceptable_valor2,    $res->id_rango_aceptable_tipo,    2);
        $estado[] = $this->evaluarEstadoRegistro($data->valor,  $res->rango_deficiente_valor,   $res->rango_deficiente_valor2,   $res->id_rango_deficiente_tipo,   3);        
        $final=[
            1=>'satisfactorio',
            2=>'aceptable', 
            3=>'deficiente',
        ];
        $res=null;
        try {
            $res=$final[max($estado)];
        } catch (\Throwable $th) {
        }
        return $res;
    }
    public function SaveRegistros(Request $request)
    {
        if($request->hasFile('file')) 
        {
            $math = new DataController();
            $fecha=json_decode($request->fecha);
            $variables=$this->CalcularVariables($request->id,json_decode($request->registro));
            $ds=DIRECTORY_SEPARATOR;
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $folder=public_path().$ds.'uploads'.$ds.$request->id.'-'.$fecha->anno.'-'.$fecha->mes.$ds;
            $url=$folder;
            $file->move($url,$name); 
            $data= new TblIndicadoresRegistros();
            $data->anno         = $fecha->anno;
            $data->mes          = $fecha->mes;
            $data->id_indicador = (int)$request->id;
            $data->observacion  = $request->observacion;
            $data->id_user      = Auth::user()->id;
            $data->url_file='uploads'.$ds.$request->id.'-'.$fecha->anno.'-'.$fecha->mes.$ds.$name;
            $value=$math->EvaluarFuncion($variables->formula,$variables->parametros);
            $data->valor=$value['value'];
            $data->save();
            $data->valor_estado = $this->valor_estado($data);
            $data->save();
            return ['validate'=>true,'url'=>$data->url_file];
        }
        else
        {
            return ['validate'=>false,'url'=>null];
        }
    }
    public function indicadoresMeses($id_indicador)
    {
        $meses           = [
            ['mes'=>1 ,'name'=>'Enero'],
            ['mes'=>2 ,'name'=>'Febrero'],
            ['mes'=>3 ,'name'=>'Marzo'],
            ['mes'=>4 ,'name'=>'Abril'],
            ['mes'=>5 ,'name'=>'Mayo'],
            ['mes'=>6 ,'name'=>'Junio'],
            ['mes'=>7 ,'name'=>'Julio'],
            ['mes'=>8 ,'name'=>'Agosto'],
            ['mes'=>9 ,'name'=>'Septiembre'],
            ['mes'=>10,'name'=>'Octubre'],
            ['mes'=>11,'name'=>'Noviembre'],
            ['mes'=>12,'name'=>'Diciembre']
        ];
        $fecha           = date('Y-m-d',strtotime(date('Y-m-d')."- 20 days"));
        $mesActual       = date('n',strtotime($fecha));
        $annoActual      = date('Y',strtotime($fecha));
        $trimestreActual = ceil($mesActual/3);
        $res             = array();
        for($i=3; $i>=1; $i--)
        {
            $mesX   = ($trimestreActual*3)-$i;
            $key    = array_search($mesX, array_column($meses, 'mes'));
            $mes    = $meses[$mesX]['mes'];
            $name   = $meses[$mesX]['name'];
        
            $dataRes         = TblIndicadoresRegistros::
            select('anno','mes')
            ->where('id_indicador','=',$id_indicador)
            ->where('anno','=',$annoActual)
            ->where('mes','=', $mes)
            ->groupBy('anno','mes')
            ->get();
            if(count($dataRes)===0)
            {
                $res[]  = array
                (
                    'id'=>['anno'=>$annoActual,'mes'=>$mes],
                    'mes'=>$mes,
                    'anno'=>$annoActual,
                    'name'=>$name
                );
            }
        }
        return $res; 
    }
    public function registros(Request $request)
    {
        $data=TblIndicadoresVariables::
        select(
         'tbl_indicadores_variables.id',
         'tbl_indicadores.numero',
         'tbl_indicadores.codigo',
         'tbl_indicadores_variables.nombre_corto',
         'tbl_indicadores_variables.nombre_largo',
         'tbl_indicadores.nombre'
        )
        ->join('tbl_indicadores','tbl_indicadores_variables.id_indicador','=', 'tbl_indicadores.id')
        ->where('tbl_indicadores.id','=',$request->id)
        ->get();
        $indicador  = TblIndicadore::find($request->id);
        $fechas     = $this->indicadoresMeses($request->id);
        return ['data'=>$data,'fechas'=>$fechas,'formula'=>$indicador->formula_texto,'formula_corta'=>$indicador->formula];
    }
    
    private function semestral($request,$id_indicador,$meses)
    {
        $cantMeses      = 12/count($meses);
        $dataInicial    = TblIndicadoresRegistros::
        select('valor','mes')->
        where('anno','=',$request->anooInicial)->
        where('id_indicador','=',$id_indicador)->
        orderBy('mes')->
        get();
        $dataFinal      = TblIndicadoresRegistros::
        select('valor','mes')->
        where('anno','=',$request->anooFinal)->
        where('id_indicador','=',$id_indicador)->
        orderBy('mes')->
        get();
        $mx=array();
        $keyI=array();
        $keyF=array();
        $valueFinal=0;
        $valueInicial=0;
        $resultado=array();
        $dataInicial=json_decode(json_encode($dataInicial));
        $dataFinal=json_decode(json_encode($dataFinal));
        foreach($meses as $key => $mes)
        {
            $mx=array();
            for($i=0;$i<$cantMeses;$i++)
            {
                $mx[]=($mes['id']*$cantMeses)-$i;
            }
            foreach($mx as $key1 => $m)
            {
                $keyI[$key1]=array_search($m, array_column($dataInicial, 'mes'));
                $keyF[$key1]=array_search($m, array_column($dataFinal, 'mes'));
            }
            foreach($keyI as $keyi=> $i)
            {
                if($i!==false)
                {
                    $valueInicial+=$dataInicial[$i]->valor;
                }
            }
            foreach($keyF as $keyf=> $f)
            {
                if($f!==false)
                {
                    $valueFinal+=$dataFinal[$f]->valor;
                }
            }
            $valueInicial   =$valueInicial/$cantMeses;
            $valueFinal     =$valueFinal/$cantMeses;
            $meses[$key]['anooInicial']= ['anno'=>$request->anooInicial,'value'=>$valueInicial];
            $meses[$key]['anooFinal']  = ['anno'=>$request->anooFinal,  'value'=>$valueFinal];
        }
        return ['data'=>$meses];
    }

    public function comparativo(Request $request)
    {
        $data = array();
        $res=TblIndicadoresRegistros::
        select('tbl_indicadores_registros.id_indicador','tbl_indicadores.nombre')->
        join('tbl_indicadores','tbl_indicadores_registros.id_indicador', '=', 'tbl_indicadores.id')-> 
        groupBy('tbl_indicadores_registros.id_indicador')->
        groupBy('tbl_indicadores.nombre')->
        get();
        foreach($res as $temp)
        {
            switch($request->informe)
            {
                case 'mensual':
                    $meses=
                    [
                        ['id'=>1,  'name'=>'Primer mes'  ],
                        ['id'=>2,  'name'=>'Segundo mes' ],
                        ['id'=>3,  'name'=>'Tercer mes'  ],
                        ['id'=>4,  'name'=>'Cuarto mes'  ],
                        ['id'=>5,  'name'=>'Quinto mes'  ],
                        ['id'=>6,  'name'=>'Sexto mes'   ],
                        ['id'=>7,  'name'=>'Séptimo mes' ],
                        ['id'=>8,  'name'=>'Octavo mes'  ],
                        ['id'=>9,  'name'=>'Noveno mes'  ],
                        ['id'=>10, 'name'=>'Décimo mes'  ],
                        ['id'=>11, 'name'=>'Onceavo mes' ],
                        ['id'=>12, 'name'=>'Doceavo mes' ]
                    ];
                break;
                case 'trimestral':
                    $meses=
                    [
                        ['id'=>1,'name'=>'Primer trimestre' ],
                        ['id'=>2,'name'=>'Segundo trimestre'],
                        ['id'=>3,'name'=>'Tercer trimestre' ],
                        ['id'=>4,'name'=>'Cuarto trimestre' ]
                    ];
                break;
                case 'semestral':
                    $meses=
                    [
                        ['id'=>1,'name'=>'Primer semestre' ],
                        ['id'=>2,'name'=>'Segundo semestre']
                    ];
                break;
                case 'anual':
                    $meses=
                    [
                        ['id'=>1,'name'=>'Primer año' ]
                    ];
                break;
                default:
                    abort(404);
                break;
            }
            $data['indicador'][]=
            [
                'value'           => $this->semestral($request,$temp->id_indicador,$meses), 
                'tbl_indicador'   => $temp
            ];
        }
        return $data;
    }
    public function AllIndicadoresTipo()
    {
        return TblIndicadoresTipo::all();
    }
    public function borrar(Request $request)
    {
        try {
            $data = TblIndicadore::findOrFail($request->id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
