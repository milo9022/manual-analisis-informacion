<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblIndicadoresTipo;
use App\Models\TblIndicadore;
use App\Models\TblIndicadoresRegistros;
use DB;

class piramideController extends Controller
{
    public function detalle(Request $request)
    {
        $valor=TblIndicadore::
            select(
                'tbl_indicadores.nombre',
                'tbl_indicadores_registros.valor',
                'tbl_indicadores_registros.anno',
                'tbl_indicadores_registros.mes',
                'tbl_indicadores_registros.valor_estado',
                'tbl_areas.nombre as area',
                'tbl_indicadores.numero',
                'users.documento',
                DB::raw('CONCAT_WS(\' \', users.nombre_primero, users.nombre_segundo, users.apellido_primero, users.apellido_segundo) AS usuario')
            )->
            where('tbl_indicadores.id_indicador_tipo','=',$request->id)->
            where('tbl_indicadores_registros.valor_estado','=',"$request->descripciones")->
            join('tbl_indicadores_registros','tbl_indicadores.id', '=', 'tbl_indicadores_registros.id_indicador')->
            join('tbl_areas','tbl_indicadores.id_area', '=', 'tbl_areas.id')->
            join('users','tbl_indicadores_registros.id_user', '=', 'users.id')->
            orderBy('tbl_indicadores.nombre')->
            orderBy('tbl_indicadores_registros.anno')->
            orderBy('tbl_indicadores_registros.mes')->
            get();
            $nombre=TblIndicadoresTipo::findOrFail($request->id);
        return ['validate'=>true,'data'=>['nombre'=>$nombre->nombre,'valor'=>$valor]];
    }
    public function index()
    {
        $total=array();
        //Primero miro los tipos de indicadores a analizar
        $tipos=TblIndicadoresTipo::select('id','nombre')->orderBy('nivel','ASC')->get();

        foreach($tipos as $key=>$temp)
        {
            $total[$key]['nombre']=$temp->nombre;
            $total[$key]['id']    =$temp->id;
            $aceptable=TblIndicadore::
            join('tbl_indicadores_registros','tbl_indicadores.id', '=', 'tbl_indicadores_registros.id_indicador')->
            where('tbl_indicadores.id_indicador_tipo','=',$temp->id)->
            where('tbl_indicadores_registros.valor_estado','=','aceptable')->
            count();

            $satisfactorio=TblIndicadore::
            join('tbl_indicadores_registros','tbl_indicadores.id', '=', 'tbl_indicadores_registros.id_indicador')->
            where('tbl_indicadores.id_indicador_tipo','=',$temp->id)->
            where('tbl_indicadores_registros.valor_estado','=','satisfactorio')->
            count();

            $deficiente=TblIndicadore::
            join('tbl_indicadores_registros','tbl_indicadores.id', '=', 'tbl_indicadores_registros.id_indicador')->
            where('tbl_indicadores.id_indicador_tipo','=',$temp->id)->
            where('tbl_indicadores_registros.valor_estado','=','deficiente')->
            count();
            
            $total[$key]['rango']['satisfactorio'] = $satisfactorio;
            $total[$key]['rango']['aceptable']     = $aceptable;
            $total[$key]['rango']['deficiente']    = $deficiente;
        }
        return ['validate'=>true,'data'=>$total];
    }
}
