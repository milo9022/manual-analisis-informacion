<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\TblConfig;

class TblConfigController extends Controller
{
    public function Get($name)
    {
        return TblConfig::select('value')->where('name','=',$name)->first();
    }
    public static function GetData($name)
    {
        $data = TblConfig::select('value')->where('name','=',$name)->first();
        return $data->value; 
    }
    public static function SetData($name, $value)
    {
        $data = TblConfig::firstOrNew(['name' => $name]);
        $data->name     = $name;
        $data->value    = $value;
        $data->save();
        return (object)['validate'=>true];
    }
    
    public function Set($name, $value)
    {
        $data = new TblConfig();
        $data->name     = $name;
        $data->value    = $value;
        $data->save();
        return ['validate'=>true];
    }
    public function borrar($id)
    {
        try {
            $data = TblConfig::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
