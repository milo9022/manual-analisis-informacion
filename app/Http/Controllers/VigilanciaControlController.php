<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblVigilanciaControl;

class VigilanciaControlController extends Controller
{
    public function AllVigilanciaControl()
    {
        return TblVigilanciaControl::all();
    }
    public function borrar($id)
    {
        try {
            $data = TblVigilanciaControl::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
