<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MathParser\StdMathParser;
use MathParser\Interpreting\Evaluator;

class DataController extends Controller
{
    public function meses(Request $request)
    {
        $meses=array(
            ['id'=>1, 'name'=>'Enero'],
            ['id'=>2, 'name'=>'Febrero'],
            ['id'=>3, 'name'=>'Marzo'],
            ['id'=>4, 'name'=>'Abril'],
            ['id'=>5, 'name'=>'Mayo'],
            ['id'=>6, 'name'=>'Junio'],
            ['id'=>7, 'name'=>'Julio'],
            ['id'=>8, 'name'=>'Agosto'],
            ['id'=>9, 'name'=>'Septiembre'],
            ['id'=>10,'name'=>'Octubre'],
            ['id'=>11,'name'=>'Noviembre'],
            ['id'=>12,'name'=>'Diciembre']
        );
        return ['validate'=>true,'data'=>$meses];
    }
    public function EvaluarFuncion($string,$parameters=[])
    {
        try 
        {
            $parser = new StdMathParser();
            $evaluator = new Evaluator();
            $AST = $parser->parse($string);
            if(count($parameters)>0)
            {
                $evaluator->setVariables($parameters);//[ 'x' => 2, 'y' => 3 ]
            }
            $value = $AST->accept($evaluator);
            return ['validate'=>true,'value'=>$value,'msj'=>null];
        }
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'value'=>null,'msj'=>$th->getMessage()];
        }
    }
    public function ValidarVariables(Request $request)
    {
        $formula=$request->formula_corta;
        $res=array();
        $registros=$request->all();
        foreach($registros['registro'] as $temp)
        {
            $res[$temp['nombre_corto']]=$temp['value'];
        }
        return $this->EvaluarFuncion($formula,$res);
    }
    public function EjecutarEvaluacion()
    {
        $string='x. y';
        $parameters=[ 'x' => 2, 'y' => 3 ];
        $resultado=$this->EvaluarFuncion($string,$parameters);
        return $resultado;
    }
    public function ValidarFuncion(Request $request)
    {
        try 
        {
            $variables=array();
            foreach($request->variables as $temp)
            {
                $variables[$temp['nombre_corto']]=5;
            }
            $r=$this->EvaluarFuncion($request->formula,$variables);
            return $r;
        }
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'value'=>null,'msj'=>$th->getMessage()];
        }
    }
}
