<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblComiteEncargadoAnalisis;

class ComiteEncargadoAnalisisController extends Controller
{
    public function AllComiteEncargadoAnalisis()
    {  
        return TblComiteEncargadoAnalisis::
        orderBy('apellid_primero')->
        orderBy('apellid_segundo')->
        orderBy('nombre_primero')->
        orderBy('nombre_segundo')->
        with('tblArea')->
        with('tblDocumentoTipo')->
        get();
    }
    public function borrar($id)
    {
        try {
            $data = TblComiteEncargadoAnalisis::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
