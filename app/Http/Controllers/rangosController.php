<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblRango;
use App\Models\TblRangosTipo;

class rangosController extends Controller
{
    public function Allrangos()
    {
        return TblRango::all();
    }
    public function AllrangosTipos()
    {
        return TblRangosTipo::all();
    }
    public function borrar($id)
    {
        try {
            $data = TblRangosTipo::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
}
