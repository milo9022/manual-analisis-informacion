<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblArea;

class areasController extends Controller
{
    public function find($id)
    {
        return ['validate'=>true,'msj'=>null,'data'=>TblArea::find($id)];
    }
    public function AllAreas(Request $request)
    {
        $data = TblArea::orderBy('nombre');
        if(isset($request->page))
        {
            try 
            {
                $limit=$request->registros;
                $registro=($limit*$request->page)-$limit;
                $data->limit($limit)->offset($registro);
                $total=TblArea::count();
                return ['data'=>$data->get(),'page'=>$request->page,'pages'=>ceil($total/$limit),'total'=>$total];
            } 
            catch (\Throwable $th)
            {
                return [];
            }
        }
        else
        {
            return $data->get();
        }
    }
    public function Save(Request $request)
    {
        try 
        {
            $TblArea = new TblArea();
            $TblArea->nombre        = $request->nombre;
            $TblArea->descripcion   = $request->descripcion;
            $TblArea->Save();
            return ['validate'=>true,'msj'=>null,'response'=>$TblArea->id];
        }
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
    public function SaveUpdate($id,Request $request)
    {
        try 
        {
            $TblArea = TblArea::find($id);
            $TblArea->nombre        = $request->nombre;
            $TblArea->descripcion   = $request->descripcion;
            $TblArea->Save();
            return ['validate'=>true,'msj'=>null,'response'=>$TblArea->id];
        }
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
    public function borrar($id)
    {
        try {
            $data = TblArea::find($id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return ['validate'=>false,'msj'=>$th->getMessage(),'response'=>null];
        }
    }
    
}
