<?php

namespace App\Http\Controllers;
use App\Models\TblPuntosAtenciones;
use Illuminate\Http\Request;

class TblPuntosAtencionesController extends Controller
{
    public function AllIndicadores(Request $request)
    {
        $data= TblPuntosAtenciones::
        orderBy('nombre');
        if(isset($request->page))
        {
            try 
            {
                $limit=$request->registros;
                $registro=($limit*$request->page)-$limit;
                $data->limit($limit)->offset($registro);
                $total=TblPuntosAtenciones::count();
                return ['data'=>$data->get(),'page'=>$request->page,'pages'=>ceil($total/$limit),'total'=>$total];
            } catch (\Throwable $th) {
                return [];
            }
        }
        else
        {
            return $data->get();
        }
    }
    public function index()
    {
        $tblPuntosAtenciones = TblPuntosAtenciones::
        select(
            'tbl_punto_atenciones.id',
            'tbl_punto_atenciones.nombre',
            'tbl_municipios.nombre AS municipio_nombre'
        )
        ->leftJoin('tbl_municipios','tbl_punto_atenciones.id_municipio', '=', 'tbl_municipios.id')
        ->orderBy('tbl_punto_atenciones.nombre')
        ->get();
        return $tblPuntosAtenciones;
    }
    public function find(Request $request)
    {
        try {
            $tblPuntosAtenciones = TblPuntosAtenciones::findOrFail($request->id);
            return ['validate'=>true,'data'=>$tblPuntosAtenciones];
        } catch (\Throwable $th) {
            return response()->json(['validate' => false,'data'=>$th->getMessage()], 403); 
        }
    }
    public function save(Request $request)
    {
        try {

            $tblPuntosAtenciones = new TblPuntosAtenciones();
            $tblPuntosAtenciones->nombre=$request->nombre;
            $tblPuntosAtenciones->id_municipio=1;
            $tblPuntosAtenciones->save();
            return ['validate'=>true,'data'=>$tblPuntosAtenciones];
        } catch (\Throwable $th) {
            return response()->json(['validate' => false,'data'=>$th->getMessage()], 403); 
        }
    }
    public function delete(Request $request)
    {
        try {
            $data = TblPuntosAtenciones::findOrFail($request->id);
            $data->delete();
            return ['validate'=>true,'msj'=>null,'response'=>'Registro borrado'];
        } catch (\Throwable $th) {
            return response()->json(['validate' => false,'data'=>$th->getMessage()], 403); 
        }
    }
    public function update(Request $request)
    {
        try {
            $tblPuntosAtenciones = TblPuntosAtenciones::findOrFail($request->id);
            $tblPuntosAtenciones->nombre=$request->nombre;
            $tblPuntosAtenciones->save();
            return ['validate'=>true,'data'=>$tblPuntosAtenciones];
        } catch (\Throwable $th) {
            response()->json(['validate' => false,'data'=>$th->getMessage()], 403); 
        }
    }
}
