<?php
Auth::routes();
Route::group(['middleware' => ['auth','validateActive']],function()
{
    Route::get('/logout','SinglePageController@cerrarSession');
    Route::get('/','SinglePageController@home');
    Route::get('/limpiar','SinglePageController@limpiar');
    Route::post('/validarResultados','DataController@ValidarVariables');
    Route::get('/meses','DataController@meses');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('/dashboard/{any}', 'SinglePageController@index')->where('any', '.*');
    Route::post('/validarFormula','DataController@ValidarFuncion');
    Route::post('/validarResultados','DataController@ValidarVariables');
    Route::get('/meses','DataController@meses');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['prefix' =>    'indicadores',], function () 
    {
        Route::get('/',          'indicadoresController@AllIndicadores');
        Route::get('/tipos',     'indicadoresController@AllIndicadoresTipo');
        Route::post('/Edit',     'indicadoresController@update');
        Route::post('/',         'indicadoresController@Save');
        Route::post('/load',     'indicadoresController@Load');
        Route::post('/registros','indicadoresController@registros');
        Route::get('/registros', 'indicadoresController@AllRegistros');
        Route::get('/comparativo', 'indicadoresController@comparativo');
        Route::post('/delete',    'indicadoresController@borrar');
        Route::post('/save',     'indicadoresController@SaveRegistros');
    });
    Route::group(['prefix' => 'piramide'], function ()
    {
        Route::get('/',        'piramideController@index');
        Route::post('/detail', 'piramideController@detalle');
    });
    Route::group(['prefix' => 'frecuenciaMedidas',], function ()
    {
        Route::get('/all', 'frecuenciaMedidasController@AllFrecuencia');
        Route::get('/delete/{id}', 'frecuenciaMedidasController@borrar');
    });
    Route::group(['prefix' => 'puntosatencion',], function ()
    {
        Route::get('/',       'TblPuntosAtencionesController@index');
        Route::post('/',      'TblPuntosAtencionesController@save');
        Route::get('/all',    'TblPuntosAtencionesController@AllIndicadores');
        Route::post('/find',  'TblPuntosAtencionesController@find');
        Route::post('/update','TblPuntosAtencionesController@update');
        Route::post('/delete','TblPuntosAtencionesController@delete');
    });
    Route::group(['prefix' => 'ComiteEncargadoAnalisis',], function ()
    {
        Route::get('/all', 'UsuariosController@usuariosAll');
        Route::get('/delete/{id}', 'ComiteEncargadoAnalisisController@borrar');
    });
    Route::group(['prefix' => 'VigilanciaControl',], function ()
    {
        Route::get('/all', 'VigilanciaControlController@AllVigilanciaControl');
        Route::get('/delete/{id}', 'VigilanciaControlController@borrar');
    });
    Route::group(['prefix' => 'areas'], function () {
        Route::get('/', 'areasController@AllAreas');
        Route::get('/edit/{id}', 'areasController@find');
        Route::post('/','areasController@Save');
        Route::post('/update/{id}','areasController@SaveUpdate');
        Route::get('/delete/{id}', 'areasController@borrar');
    });
    Route::group(['prefix' => 'rangos'], function () {
        Route::get('/all', 'rangosController@Allrangos');
        Route::get('/tipos', 'rangosController@AllrangosTipos');
        Route::get('/delete/{id}', 'rangosController@borrar');
        
    });
    Route::group(['prefix' => 'usuarios'], function () {
        
        Route::post('/newsave','UsuariosController@newsave');
        Route::post('/update','UsuariosController@update');
        Route::post('/bloquear','UsuariosController@bloquear');
        Route::post('/restablecer','UsuariosController@restablecer');
        
        Route::get('/roles','UsuariosController@rolesAll');
        Route::get('/edit/{id}','UsuariosController@usuariosFind');
        Route::post('/editSave/{id}','UsuariosController@editSave');
        Route::get('/list','UsuariosController@usuariosAll');
        Route::get('/selected','UsuariosController@usuarioActual');
        Route::get('/permistions','AuthController@permistions');
        Route::post('/permistions/all','AuthController@permistionsAll');
        Route::post('/permistions/save','AuthController@permistionsSave');
        Route::post('/changepass','UsuariosController@changePass');
        Route::post('/delete', 'UsuariosController@borrar');
    });
});