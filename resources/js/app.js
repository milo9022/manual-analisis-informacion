require('./bootstrap');
import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import Vuetify from 'vuetify';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Routes from './routes/pages';
import App from './pages/App';
import BootstrapVue from 'bootstrap-vue'
import JsonExcel from 'vue-json-excel'


Vue.component('downloadExcel', JsonExcel)
Vue.use(VueAxios, axios);
Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.use(Vuetify);


const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App)
});

export default app;