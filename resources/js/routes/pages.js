import Vue from 'vue';
import VueRouter from 'vue-router';

import indicadoresIndex from '../views/indicador/index';
import indicadoresNew from '../views/indicador/new';
import indicadoresEdit from '../views/indicador/edit';


import areasIndex from  '../views/areas/index';
import areasNew from    '../views/areas/new';
import areasEdit from   '../views/areas/edit';

import puntosatencionIndex from '../views/puntosatencion/index';
import puntosatencionNew   from '../views/puntosatencion/create';
import puntosatencionEdit  from '../views/puntosatencion/update';


import piramide_index from   '../views/piramide/index';
import piramide_detail from  '../views/piramide/detail';



import registrarIndicador from '../views/registro/new';
import registrarIndex from '../views/registro/index';
import PageNotFound from '../pages/error404'

import comparativo    from '../views/comparativos/index';
import comparativoTrimestral    from '../views/comparativos/trimestral';

import Users from '../views/user/Users'
import UsersEdit from '../views/user/UsersEdit'
import UsersNew from '../views/user/UsersNew'
import password from '../views/user/changepass';

import start from '../pages/start'
import logout from '../pages/logout'
import noAutorizate from '../pages/noAutorizate'
import permisos from '../views/user/permisions'

const prefix = '/dashboard';
Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: prefix + '/start',
            name: 'start',
            component: start
        },
        {
            path: prefix + '/logout',
            name: 'logout',
            component: logout
        },
        {
            path: prefix + '/areas',
            name: 'areasindex',
            component: areasIndex,
            meta: {
                auth: true,
                title:'Areas',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/areas/edit/:id',
            name: 'areasedit',
            component: areasEdit,
            meta: {
                auth: true,
                title:'Editar area',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/areas/new',
            name: 'areasnew',
            component: areasNew,
            meta: {
                auth: true,
                title:'Nueva area',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/index',
            name: 'indicadorindex',
            component: indicadoresIndex,
            meta: {
                auth: true,
                title:'Indicadores',
                roles:['user','admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/new',
            name: 'indicadornew',
            component: indicadoresNew,
            meta: {
                auth: true,
                title:'Nuevo indicador',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/edit/:id',
            name: 'indicadorEdit',
            component: indicadoresEdit,
            meta: {
                auth: true,
                title:'Editar indicador',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/puntosatencion',
            name: 'puntosatencion_index',
            component: puntosatencionIndex,
            meta: {
                auth: true,
                title:'Puntos de atención',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/puntosatencion/new',
            name: 'puntosatencionnew',
            component: puntosatencionNew,
            meta: {
                auth: true,
                title:'Nuevo punto de atención',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/puntosatencion/edit/:id',
            name: 'puntosatencionEdit',
            component: puntosatencionEdit,
            meta: {
                auth: true,
                title:'Editar punto de atención',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/comparativo/trimestral',
            name: 'comparativoTrimestral',
            component: comparativoTrimestral,
            meta: {
                auth: true,
                title:'Comparativos',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/comparativo',
            name: 'comparativo',
            component: comparativo,
            meta: {
                auth: true,
                title:'Comparativo',
                roles:['admin','supervisor','observador']
            }
        },
        //Piramide
        {
            path: prefix + '/resumen',
            name: 'piramide_index',
            component: piramide_index,
            meta: {
                auth: true,
                title:'Resumen',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/resumen/:descripciones/:id',
            name: 'piramide_detail',
            component: piramide_detail,
            meta: {
                auth: true,
                title:'Detalle del resumen',
                roles:['admin','supervisor','observador']
            }
        },

        //Piramide
        {
            path: prefix + '/registro/:id',
            name: 'registrosNew',
            component: registrarIndicador,
            meta: {
                auth: true,
                title:'Nuevo registro',
                roles:['admin','supervisor','user','observador']
            }
        },
        {
            path: prefix + '/registro/',
            name: 'registrosindex',
            component: registrarIndex,
            meta: {
                auth: true,
                title:'Registros',
                roles:['user','admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios',
            name: 'users',
            component: Users,
            component: Users,
            meta: {
                auth: true,
                title:'Usuarios',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/edit/:id',
            name: 'usersedit',
            component: UsersEdit,
            meta: {
                auth: true,
                title:'Editar usuario',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/nuevo',
            name: 'usersnew',
            component: UsersNew,
            meta: {
                auth: true,
                title:'Nuevo usuario',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/cambiarpass',
            name: 'cambiarpass',
            component: password,
            meta: {
                auth: true,
                title:'Cambiar contraseña',
                roles:['user','admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/permisos',
            name: 'permisos',
            component: permisos,
            meta: {
                auth: true,
                title:'Permisos de usuario',
                roles:['admin']
            }
        },
        { path: "*", name: 'error404',     component: PageNotFound, meta: { auth: false } },
        { path: prefix + "/accesodenegado", name: 'noAutorizate', component: noAutorizate, meta: { auth: false } }
    ]
});
router.beforeEach((to, from, next) => 
{
    //Validar roles
    if(to.name!='start' && to.name!='logout' && to.name!='noAutorizate')
    {
        let validate=false;
        try{validate = to.meta.roles.indexOf(localStorage.getItem('rol'))>-1;}
        catch(e){}
        if(validate) 
        {
            next()
        }
        else
        {
            next({name: 'noAutorizate',params: { nextUrl: to.fullPath }})
        }
    }
    //Validar roles
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
    if(!nearestWithMeta) return next();
    nearestWithMeta.meta.metaTags.map(tagDef =>
    {
        const tag = document.createElement('meta');
        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });
        tag.setAttribute('data-vue-router-controlled', '');
        return tag;
    })
    .forEach(tag => document.head.appendChild(tag));
    next();
});


export default router;